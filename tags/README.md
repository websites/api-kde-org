# How to generate tags files

Tag files are used to automate linking to Qt API in https://develop.kde.org/docs and to generate dot dependency graphs in https://api.kde.org.

The simplest way to generate these files is to use the super repo which builds everything together with Qt packages from your distribution, including qt6-tools, qt6-tools-qdoc, qt6-test-private-devel, qt6-quicktest-private-devel and qt6-widgets-private-devel or equivalent in your distribution.

```bash
git clone https://code.qt.io/qt/qt5.git --branch 6.6 --depth 1
mkdir tags
cd qt5
./init-repository
cmake -B build -G Ninja -DQT_HOST_PATH=/usr -DQT_NO_PACKAGE_VERSION_CHECK=TRUE -DQT_NO_PACKAGE_VERSION_INCOMPATIBLE_WARNING=TRUE
cmake --build build --target docs
cp $(find . -name "*.tags") ../tags/
rm ../tags/Makefile.tags
rm ../tags/qmake.tags
rm ../tags/testtagfile.tags
```

After this is run, all tags should be copied to `tags/`.

QT_HOST_PATH can be the path to self-built Qt or installed via Qt's online installer.

QT_NO_PACKAGE_VERSION_CHECK disables the check for whether the installed Qt dependencies match the version we are attempting to build (for example, using Qt 6.6.2 dependencies for a Qt 6.6.3 build).
This check is only relevant when compiling the default target.

QT_NO_PACKAGE_VERSION_INCOMPATIBLE_WARNING removes the noisy warning that we are using QT_NO_PACKAGE_VERSION_CHECK.
